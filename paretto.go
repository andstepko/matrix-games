package main

func paretto(strategies []Strategy, bigBetter bool) (optimal []int) {
	removedIndexes := make(map[int]struct{})

	for k, sk := range strategies {
		if _, ok := removedIndexes[k]; ok {
			// k index is already deleted (it's non-optimal by Paretto)
			continue
		}

		for l, sl := range strategies {
			if _, ok := removedIndexes[l]; ok {
				// l index is already deleted (it's non-optimal by Paretto)
				continue
			}
			if l == k {
				// no point to compare with itself
				continue
			}

			r := parettoCmp(sk.Results, sl.Results)

			if (r == -1 && bigBetter) || (r == 1 && !bigBetter) {
				// k is worse than l - delete k
				removedIndexes[k] = struct{}{}
				break
			}

			if (r == 1 && bigBetter) || (r == -1 && !bigBetter) {
				// k is better than l - delete l
				removedIndexes[l] = struct{}{}
				continue
			}
		}
	}

	var result []int
	for i, _ := range strategies {
		if _, ok := removedIndexes[i]; !ok {
			result = append(result, i)
		}
	}
	return result
}

func parettoCmp(l1, l2 []int) int {
	if len(l1) != len(l2) {
		panic("Only slices of same length can be tried to be compared by Paretto.")
	}

	var result int

	for i := range l1 {
		if l1[i] > l2[i] {
			if result == -1 {
				return 0
			}

			result = 1
			continue
		}

		if l1[i] < l2[i] {
			if result == 1 {
				return 0
			}

			result = -1
			continue
		}
	}

	return result
}
