package main

import (
	"fmt"
	"bufio"
	"os"
	"strconv"
	"math/rand"
)

var (
	sprls, _ = NewGame([]Strategy{
		NewStrategy("scissors", []int{0, 1, -1, 1, -1}),
		NewStrategy("paper",    []int{-1, 0, 1, -1, 1}),
		NewStrategy("rock",     []int{1, -1, 0, 1, -1}),
		NewStrategy("lizard",   []int{-1, 1, -1, 0, 1}),
		NewStrategy("spock",    []int{1, -1, 1, -1, 0}),
	}, []string{"scissors", "paper", "rock", "lizard", "spock"})

	g, _ = NewGame([]Strategy{
		NewStrategy("A1", []int{9, 12, 8, 11, 14}),
		NewStrategy("A2", []int{13, 14, 13, 16, 15}),
		NewStrategy("A3", []int{16, 10, 12, 9, 13}),
		NewStrategy("A4", []int{12, 11, 9, 15, 10}),
	}, []string{"B1", "B2", "B3", "B4", "B5"})
)

func main() {
	play(sprls)

	//play(g)
}

func play(game Game) {
	if game.FirstStrategies == nil {
		// TODO Get from user - only symmetric?
		panic("Not implemented yet.")
	}

	roundCount := 1
	for {
		fmt.Println()
		fmt.Println()
		fmt.Println("-----------------------NEW ROUND-----------------------")

		playRound(game, roundCount)
		roundCount++
	}
}

func playRound(game Game, roundNumber int) {
	fmt.Println("Player 1, choose your strategy (use r - for random, o - for optimal):")
	fStrategy := readAndChooseStrategy(game, false)
	fmt.Println("Player 1 chose Strategy", game.FirstStrategies[fStrategy])

	fmt.Println()
	fmt.Println("Player 2, choose your strategy (use r - for random, o - for optimal):")
	sStrategy := readAndChooseStrategy(game, true)
	fmt.Println("Player 2 chose Strategy", game.SecondStrategies[sStrategy])

	fmt.Printf("Round %d results:\n", roundNumber)
	roundResult := game.FirstStrategies[fStrategy].Results[sStrategy]
	fmt.Println()
	if roundResult > 0 {
		fmt.Println("Player 1 won.")
	} else if roundResult == 0 {
		fmt.Println("Drawn.")
	} else {
		fmt.Println("Player 2 won.")
	}
	fmt.Println("Player 1 result is:", roundResult)
	fmt.Println("Player 2 result is:", -roundResult)
}

func readAndChooseStrategy(game Game, secondPlayer bool) int {
	reader := bufio.NewReader(os.Stdin)
	var strategies []Strategy
	if secondPlayer {
		strategies = game.SecondStrategies
	} else {
		strategies = game.FirstStrategies
	}

	for i, s := range strategies {
		fmt.Printf("%d: %s\n", i, s.Name)
	}

	for {
		text, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println(err.Error())
			continue
		}
		text = text[:len(text)-1]

		if text == "info" {
			printGameInfo(game)
			continue
		}
		if text == "o" {
			if game.CleanNashOptimalExist() {
				fmt.Println("Choosing clean Nash-optimal strategy.")
				if secondPlayer {
					return game.GetSecondMinMaxStrategy()
				} else {
					return game.GetFirstMaxMinStrategy()
				}
			} else {
				fmt.Println("Clean Nash-optimal strategy does not exist. Choosing Strategy among Paretto-optimal.")
				var parettoIndexes []int
				if secondPlayer {
					parettoIndexes = game.SecondParettoOptimal()
				} else {
					parettoIndexes = game.FirstParettoOptimal()
				}

				index := rand.Intn(len(parettoIndexes))
				return parettoIndexes[index]
			}
		}
		if text == "r" {
			return rand.Intn(len(strategies))
		}

		i, err := strconv.Atoi(text)
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		if i < 0 || i >= len(strategies) {
			fmt.Printf("Strategy number must be in range [0;%d]", len(strategies)-1)
			continue
		}

		return i
	}
}

func printGameInfo(game Game) {
	// TODO Print gaming matrix

	fmt.Printf("\nParetto optimal Strategies for Player 1 (%d):\n", len(game.FirstParettoOptimal()))
	for _, sIndex := range game.FirstParettoOptimal() {
		fmt.Println(game.FirstStrategies[sIndex])
	}

	fmt.Printf("\nParetto optimal Strategies for Player 2 (%d):\n", len(game.SecondParettoOptimal()))
	for _, sIndex := range game.SecondParettoOptimal() {
		fmt.Println(game.SecondStrategies[sIndex])
	}

	fmt.Println()
	fmt.Printf("Low Game price is: %d\n", game.GetLowGamePrice())
	fmt.Printf("High Game price is: %d\n", game.GetHighGamePrice())
	if game.GetLowGamePrice() == game.GetHighGamePrice() {
		fmt.Printf("Game price is: %d (saddle point)\n", game.GetLowGamePrice())
		_, fClean := game.GetFirstCleanNashStrategy()

		fmt.Println()
		fmt.Println("Player 1 Nash-optimal clean strategy:", fClean)
		_, sClean := game.GetSecondCleanNashStrategy()
		fmt.Println("Player 2 Nash-optimal clean strategy:", sClean)
	} else {
		fmt.Printf("Game price is between %d and %d\n", game.GetLowGamePrice(), game.GetHighGamePrice())
	}

	// TODO Nash in mixed strategies
}
