package main

import (
	"gitlab.com/distributed_lab/logan/v3/errors"
	"math"
)

type Game struct {
	FirstStrategies  []Strategy
	SecondStrategies []Strategy

	lowGamePrice  *int
	highGamePrice *int

	firstMaxMinStrategy  *int
	secondMinMaxStrategy *int

	firstParetto []int
	secondParetto []int
}

func NewGame(firstStrategies []Strategy, secondStratNames []string) (Game, error) {
	for _, s := range firstStrategies {
		if len(s.Results) != len(secondStratNames) {
			return Game{}, errors.Errorf("Expected the (%s) strategy results number to be (%d), but got (%d).",
				s.Name, len(secondStratNames), len(s.Results))
		}
	}

	var second [][]int
	for range secondStratNames {
		t := make([]int, len(firstStrategies), len(firstStrategies))
		second = append(second, t)
	}

	for i, fs := range firstStrategies {
		for j, v := range fs.Results {
			second[j][i] = v
		}
	}

	var secondStrategies []Strategy
	for i, _ := range secondStratNames {
		secondStrategies = append(secondStrategies, Strategy{
			Name:    secondStratNames[i],
			Results: second[i]})
	}

	return Game{
		FirstStrategies:  firstStrategies,
		SecondStrategies: secondStrategies,
	}, nil
}

func (g *Game) CleanNashOptimalExist() bool {
	return g.GetLowGamePrice() == g.GetHighGamePrice()
}

func (g *Game) GetLowGamePrice() int {
	if g.lowGamePrice != nil {
		return *g.lowGamePrice
	}

	var maxMin = math.MinInt64
	var maxMinStrategy = -1

	for i, fs := range g.FirstStrategies {
		strategyMin := fs.GetMin()

		if strategyMin > maxMin {
			maxMin = strategyMin
			maxMinStrategy = i
		}
	}

	g.lowGamePrice = &maxMin
	g.firstMaxMinStrategy = &maxMinStrategy
	return *g.lowGamePrice
}

func (g *Game) GetHighGamePrice() int {
	if g.highGamePrice != nil {
		return *g.highGamePrice
	}

	var minMax = math.MaxInt64
	var minMaxStrategy = -1

	for i, ss := range g.SecondStrategies {
		strategyMax := ss.GetMax()

		if strategyMax < minMax {
			minMax = strategyMax
			minMaxStrategy = i
		}
	}

	g.highGamePrice = &minMax
	g.secondMinMaxStrategy = &minMaxStrategy
	return *g.highGamePrice
}

func (g *Game) GetFirstMaxMinStrategy() int {
	g.GetLowGamePrice()
	return *g.firstMaxMinStrategy
}

func (g *Game) GetSecondMinMaxStrategy() int {
	g.GetHighGamePrice()
	return *g.secondMinMaxStrategy
}

func (g *Game) FirstParettoOptimal() []int {
	if g.firstParetto != nil {
		return g.firstParetto
	}

	g.firstParetto = paretto(g.FirstStrategies, true)
	return g.firstParetto
}

func (g *Game) SecondParettoOptimal() []int {
	if g.secondParetto != nil {
		return g.secondParetto
	}

	g.secondParetto = paretto(g.SecondStrategies, false)
	return g.secondParetto
}

func (g *Game) GetFirstCleanNashStrategy() (int, *Strategy) {
	if !g.CleanNashOptimalExist() {
		// No saddle point - thus no Nash-optimal strategy in clean strategies
		return -1, nil
	}

	return *g.firstMaxMinStrategy, &g.FirstStrategies[*g.firstMaxMinStrategy]
}

func (g *Game) GetSecondCleanNashStrategy() (int, *Strategy) {
	if !g.CleanNashOptimalExist() {
		// No saddle point - thus no Nash-optimal strategy in clean strategies
		return -1, nil
	}

	return *g.secondMinMaxStrategy, &g.SecondStrategies[*g.secondMinMaxStrategy]
}
