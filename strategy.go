package main

import (
	"math"
	"fmt"
)

type Strategy struct {
	Name    string
	Results []int

	max *int
	min *int
}

func NewStrategy(name string, results []int) Strategy {
	return Strategy{
		Name:    name,
		Results: results,
	}
}

func (s *Strategy) GetMax() int {
	if s.max != nil {
		return *s.max
	}

	var max = math.MinInt64

	for _, v := range s.Results {
		if v > max {
			max = v
		}
	}

	s.max = &max
	return *s.max
}

func (s *Strategy) GetMin() int {
	if s.min != nil {
		return *s.min
	}

	var min = math.MaxInt64

	for _, v := range s.Results {
		if v < min {
			min = v
		}
	}

	s.min = &min
	return *s.min
}

func (s Strategy) String() string {
	return fmt.Sprintf(`%s: %v`, s.Name, s.Results)
}
